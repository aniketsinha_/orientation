# Git

## What is Git?
Git is an example of _Distributed Version Control System(DVCS)_, which means that rather having a single place history, every developer for a project can maintain the history for his part of the project.

>Git makes collaboration with teammmates simple.

## Git Vocabulary

* **Repository** - A repository or "repo" is a directory or storage space for your projects.
* **Commit** - A commit is like saving a file with an unique ID assigned to it so that it can be revised easily.
* **Push** - Git push is the command which lets you transfer commits from your local to a remote repository.
* **Branch** - Each repo may have different branches like branches of a tree which eventually gets merged  back into the master branch.
* **Merge** - Git merge command lets you to integrate a single branch to the master branch.
* **Clone** -Cloning is the process of downloading a copy of source code into your local machine.
* **Fork** -Fork is production of a personal copy of someone else's repository.
## Git Workflow
![git_workflow](../../images/git_workflow.png)
